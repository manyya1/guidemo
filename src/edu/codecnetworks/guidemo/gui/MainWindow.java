/**
 * 
 */
package edu.codecnetworks.guidemo.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * @author manyaagrawal
 *
 */
public class MainWindow  //implements ActionListener
	{

	/**
	 * 
	 */
	JFrame frame= new JFrame("Title");
	JPanel[] panel= new JPanel[4];
	JLabel[] label= new JLabel[3];
	JTextField[] textField= new JTextField[3];
	JButton submitButton= new JButton("Submit");
    
	public MainWindow() {
		// TODO Auto-generated constructor stub
		submitButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				System.out.println("Button was pressed");
			}
			
		});
		for(int i=0;i<label.length;++i)
		{
			label[i] = new JLabel();
			textField[i]= new JTextField(10);
		}
		for(int i=0; i<panel.length;i++)
		{
			panel[i]= new JPanel();
			panel[i].setLayout(new BoxLayout(panel[i],BoxLayout.X_AXIS));
		}
		label[0].setText("Enter Dog's name");
		label[1].setText("Enter Dog's height");
		label[2].setText("Enter Dog's number of teeth");
		for(int i=0; i<label.length; ++i)
		{
		panel[i].add(label[i]);
		panel[i].add(textField[i]);
		panel[i].setBackground(Color.pink);
		}
		panel[3].setLayout(new BoxLayout(panel[3],BoxLayout.Y_AXIS));
		panel[3].add(panel[0]);
		panel[3].add(panel[1]);
		panel[3].add(panel[2]);
		panel[3].add(submitButton);
		
		frame.getContentPane().add(panel[3]);   //changes on the frame
		frame.setSize(300,300);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		}
	}
	